# PushOver Notifier

This simple script is written to replace PullOver client that is not maintained anymore and has bug on my fedora

## Configuration

Copy `config.ini.sample` in `config.ini`, fill `email` & `password` and you can start it

## Run it

Written to run on python3 (tested on 3.7.7).

```bash
python index.py
```

## Development

My favorite command is

```bash
ls | entr -r python index.py
```

* `ls`: list files to watch
* `entr`: rely on `inotify` api to restart the command provided
* `-r`: force cut off and restart `python` even it still running (it's a websocket client so long living)
* `python index.py`: command used to run it

## Installation

Still to do. I think that I'll create a desktop file and Makefile to simplify installation.

## Todo

* use icon provided
* use priority
* use sound
* use url to create clickable button
* handle html
* ack message
* conform to 'Distribution Guidelines'
* create desktop file. Easy way to install this script
* handle http result codes
