import gi
import asyncio
import websockets
import logging
import pushoverclient
from systemd import journal

logging.basicConfig(level=logging.INFO)
logging.root.addHandler(journal.JournalHandler())

gi.require_version('Notify', '0.7')

from gi.repository import Notify

async def consume(websocket, secret, device_id) -> None:
    # https://pushover.net/api/client#websocket
    async for byteReceived in websocket:
        logging.debug(byteReceived)
        if byteReceived == b'!':
            messages = pushoverclient.sync_messages(secret, device_id)
            for message in messages:
                notify_on_message(message)

    # ! - A new message has arrived; you should perform a sync.
    # # - Keep-alive packet, no response needed.
    # R - Reload request; you should drop your connection and re-connect.
    # E - Error; a permanent problem occured and you should not automatically re-connect. Prompt the user to login again or re-enable the device.

async def establish_websocket(secret, device_id):
    uri = "wss://client.pushover.net/push"
    async with websockets.connect(uri) as websocket:
        # login process
        await websocket.send('login:' + device_id + ':'+ secret + '\n')
        # wait for any message
        await consume(websocket, secret, device_id)

def notify_on_message(message):
    title = ''
    if 'title' not in message:
        if 'app' in message:
            title = message['app']
        else:
            title = message['message'][:8] + '...'
        logging.error(message)
    else:
        title = message['title']

    notif=Notify.Notification.new(title, message['message'])
    notif.show()
    # message['icon'], # https://api.pushover.net/icons/<icon name>.png

if __name__ == "__main__":
    Notify.init ("Pushover")

    secret = pushoverclient.retrieve_secret()
    device_id = pushoverclient.retrieve_device_id(secret)
    messages = pushoverclient.sync_messages(secret, device_id)
    # retrieve messages stacked when client offline
    for message in messages:
        notify_on_message(message)
    else:
        notify_on_message({'title': 'PushOver Notifier', 'message': 'is started and ready to receive message'})

    # launch websocket loop
    asyncio.get_event_loop().run_until_complete(establish_websocket(secret, device_id))

    logging.debug('END OF LOOP')

    Notify.uninit()
