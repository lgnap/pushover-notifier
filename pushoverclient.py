import logging
import configparser
import requests
import os

configfile_name = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'config.ini'
)

if not os.path.isfile(configfile_name):
    logging.error(f'You must create {configfile_name}')
    exit(1)

config = configparser.ConfigParser()
config.read(configfile_name)
sections = config.sections()
if 'initial' not in sections:
    logging.error('You must create initial section')
    exit(2)

if 'email' not in config['initial'] or not config['initial']['email']:
    logging.error('You must provide initial.email')
    exit(3)

if 'password' not in config['initial'] or not config['initial']['password']:
    logging.error('You must provide initial.password')
    exit(4)

def retrieve_secret():
    logging.debug('retrieve_secret')

    if 'computed' in sections and 'secret' in config['computed']:
        return config['computed']['secret']

    logging.debug('login call')
    r = requests.post(
        'https://api.pushover.net/1/users/login.json',
        data = {
            'email': config['initial']['email'],
            'password': config['initial']['password']
        }
    )
    response = r.json()
    logging.debug(response)

    if response['status'] != 1:
        logging.error('error on login')
        logging.error(response)
        exit()
    secret = response['secret']

    config['computed'] = {'secret': secret}

    with open(configfile_name, 'w') as configfile:
        config.write(configfile)

    return secret

def retrieve_device_id(secret):
    logging.debug('retrieve_device_id')
    logging.debug({secret})

    if 'computed' in sections and 'device_id' in config['computed']:
        return config['computed']['device_id']

    logging.debug('device call')
    pload = {'secret': secret,'name': config['initial']['device_name'], 'os': 'O'}
    r = requests.post('https://api.pushover.net/1/devices.json', data = pload)
    response = r.json()
    logging.debug(response)

    if response['status'] != 1:
        logging.error('error on device')
        if response['errors']['name'][0] == 'has already been taken':
            logging.error('Already registered device, drop it from pushover.net')
        else:
            logging.error(response)
        exit()

    device_id = response['id']
    config['computed']['device_id'] = device_id

    with open(configfile_name, 'w') as configfile:
        config.write(configfile)

    return device_id

def retrieve_messages(secret, device_id):
    logging.debug('retrieve_messages')
    logging.debug({secret, device_id})

    pload = {'secret': secret,'device_id': device_id}
    r = requests.get('https://api.pushover.net/1/messages.json', data = pload)
    response = r.json()
    logging.debug(response)

    if response['status'] != 1:
        logging.error('error on retrieve')
        logging.error(response)
        exit()

    messages = response['messages']

    return messages

def update_higest_message(secret, device_id, message_id):
    logging.debug('update_higest_message')
    logging.debug({
        secret,
        device_id,
        message_id
    })

    pload = {'secret': secret,'message': message_id}
    r = requests.post('https://api.pushover.net/1/devices/' + device_id + '/update_highest_message.json', data = pload)
    response = r.json()
    logging.debug(response)

    if response['status'] != 1:
        logging.error('error on higest message')
        logging.error(response)
        exit()

def sync_messages(secret, device_id):
    messages = retrieve_messages(secret, device_id)

    higest_message_id = 0
    for message in messages:
        logging.info(message)
        if message['id'] > higest_message_id: # to avoid get the last one (maybe up with high prio)
            higest_message_id = message['id']

    if higest_message_id != 0: # 0 is the default value, no msg received
        update_higest_message(secret, device_id, higest_message_id)
    return messages